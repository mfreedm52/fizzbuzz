import unittest
import FizzBuzzKata

class Test_FizzBuzzTests(unittest.TestCase):
	def test_efficient(self):
		fizz = FizzBuzzKata.FizzBuzzEngine()
		fizz.run_efficient()

if __name__ == '__main__':
    unittest.main()
