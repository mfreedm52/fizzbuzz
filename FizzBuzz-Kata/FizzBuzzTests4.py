import unittest
import FizzBuzzKata

class Test_FizzBuzzTests(unittest.TestCase):
	def test_implementations_are_equal(self):
		fizz = FizzBuzzKata.FizzBuzzEngine()
		fizzBuzzList = fizz.run_as_list()
        fizzBuzzListEff = fizz.run_as_list_efficient()
        self.assertEqual(fizzBuzzList, fizzBuzzListEff)

if __name__ == '__main__':
    unittest.main()
