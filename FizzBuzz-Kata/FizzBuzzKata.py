class FizzBuzzEngine:
    def run(self):
        for x in range(1, 100):
            if x % 3 == 0 and x % 5 == 0:
                print("FizzBuzz")
            elif x % 3 == 0:
                print("Fizz")
            elif x % 5 == 0:
                print("Buzz")
            else:
                print(x)

    def run_efficient(self):
        for x in range(1, 100):
            if x % 3 != 0 and x % 5 != 0:
                print(x)
            elif x % 3 != 0:
                print("Buzz")
            elif x % 5 != 0:
                print("Fizz")
            else:
                print("FizzBuzz")

    def run_as_list(self):
        mylist = []
        for x in range(1, 100):
            if x % 3 == 0 and x % 5 == 0:
                mylist.insert(x-1, "FizzBuzz")
            elif x % 3 == 0:
                mylist.insert(x-1, "Fizz")
            elif x % 5 == 0:
                mylist.insert(x-1, "Buzz")
            else:
                mylist.insert(x-1, x)
        return mylist

    def run_as_list_efficient(self):
        mylist = []
        for x in range(1, 100):
            if x % 3 != 0 and x % 5 != 0:
                mylist.insert(x-1, x)
            elif x % 3 != 0:
                mylist.insert(x-1, "Buzz")
            elif x % 5 != 0:
                mylist.insert(x-1, "Fizz")
            else:
                mylist.insert(x-1, "FizzBuzz")
        return mylist